package paczka;

public class Main {

    public static void main(String[] args) {
        StackOperations stack = new Stack();

        System.out.println(stack.get());
        System.out.println(stack.pop());

        stack.push("Witam");
        stack.push("ladny");
        stack.push("dzis");
        stack.push("dzien");
        stack.push("prosze");
        stack.push("szanownego");
        stack.push("pana");

        System.out.println(stack.get());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.get());
    }
}
